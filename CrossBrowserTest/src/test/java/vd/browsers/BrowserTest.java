package vd.browsers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


public class BrowserTest {

	  WebDriver driver;
	  
	  @Test
	public void crossBrowser() throws InterruptedException {
		driver.get("https://www.google.com");
	    Thread.sleep(4000);
	    driver.get("https://www.google.com");
		WebElement element = driver.findElement(By.name("q"));
	    element.sendKeys("Selenium");
		element.submit();
		System.out.println("Search is sucessfull");
		driver.quit();
	}  
	  
	@Parameters("browserName")
	  @BeforeTest
	  public void setup(String browserName) throws Exception {
		  
		  System.out.println("Browser name is : " +browserName);
		  System.out.println("Thread ID is : " +Thread.currentThread().getId()); 
		  
	     if(browserName.equalsIgnoreCase("chrome"))
		  {
		  	  System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
			  driver = new ChromeDriver();
		  }
		  
		  else if(browserName.equalsIgnoreCase("firefox"))
		  {
		   	  System.setProperty("webdriver.gecko.driver", "./Drivers/geckodriver.exe");
			  driver = new FirefoxDriver();
		  }
		 
		  else if(browserName.equalsIgnoreCase("Edge"))
		  {
		   	  System.setProperty("webdriver.edge.driver", "./Drivers/msedgedriver.exe");
			  driver = new EdgeDriver();
		  }
	  }
 
}


